for l in pgsql-admin pgsql-advocacy pgsql-announce pgsql-bugs pgsql-docs pgsql-general pgsql-interfaces pgsql-jobs pgsql-novice pgsql-performance pgsql-php pgsql-sql pgsql-students pgsql-cluster-hackers pgsql-committers pgsql-hackers pgsql-rrreviewers pgsql-www pgadmin-hackers pgadmin-support pgsql-jdbc pgsql-odbc pgsql-pkg-debian pgsql-pkg-yum psycopg pgsql-benchmarks pgsql-chat pgsql-cygwin pgsql-hackers-pitr pgsql-hackers-win32 pgsql-patches pgsql-ports pgsql-testers; do
	../archie/bin/download.py --lists $l > $l.log 2>&1 &
done

wait
