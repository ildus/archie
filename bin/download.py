#!/usr/bin/env python3

import aiohttp
import asyncio
import argparse
import os
import re

user_lists = [
    'pgsql-admin', 'pgsql-advocacy', 'pgsql-announce', 'pgsql-bugs',
    'pgsql-docs', 'pgsql-general', 'pgsql-interfaces', 'pgsql-jobs',
    'pgsql-novice', 'pgsql-performance', 'pgsql-php', 'pgsql-sql',
    'pgsql-students'
]
dev_lists = [
    'pgsql-cluster-hackers', 'pgsql-committers', 'pgsql-hackers',
    'pgsql-rrreviewers', 'pgsql-www'
]
project_lists = [
    'pgadmin-hackers', 'pgadmin-support', 'pgsql-jdbc', 'pgsql-odbc',
    'pgsql-pkg-debian', 'pgsql-pkg-yum', 'psycopg'
]
inactive_lists = [
    'pgsql-benchmarks', 'pgsql-chat', 'pgsql-cygwin', 'pgsql-hackers-pitr',
    'pgsql-hackers-win32', 'pgsql-patches', 'pgsql-ports', 'pgsql-testers'
]


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Fetch and index PostgreSQL mailing list archives')

    parser.add_argument(
        '--all-lists',
        dest='all_lists',
        action='store_true',
        help='all lists (user, project, devel, ...)')
    parser.add_argument(
        '--user-lists',
        dest='user_lists',
        action='store_true',
        help='user lists (pgsql-admin, pgsql-general, ...)')
    parser.add_argument(
        '--devel-lists',
        dest='dev_lists',
        action='store_true',
        help='developer lists (pgsql-hackers, pgsql-www, ...)')
    parser.add_argument(
        '--project-lists',
        dest='project_lists',
        action='store_true',
        help='project lists (pgadmin-hackers, pgsql-jdbc, ...)')
    parser.add_argument(
        '--inactive-lists',
        dest='inactive_lists',
        action='store_true',
        help='inactive lists (pgsql-benchmarks, pgsql-patches, ...)')
    parser.add_argument(
        '--lists',
        dest='lists',
        nargs='+',
        metavar='LIST',
        action='store',
        help='other lists')

    return parser.parse_args()


async def fetch(session, url):
    while True:
        try:
            async with session.get(url) as response:
                return await response.text()
        except aiohttp.client_exceptions.ClientPayloadError:
            continue
        except UnicodeDecodeError:
            pass

        break


async def list_of_mboxes(session, mailing_list):
    mboxes = []
    resp = await fetch(session, 'http://www.postgresql.org/list/%s/' % (mailing_list))
    for f in re.findall('%(list)s.[0-9]{6}' % {'list': mailing_list}, resp):
        mboxes.append(f)

    return mboxes


async def fetch_file(session, url, filename):
    r = await fetch(session, url)
    if r is not None:
        with open(filename, 'w') as f:
            f.write(r)
        print(filename, "fetched")
    else:
        print(filename, "failed")


async def download_mboxes(mailing_list, mboxes):
    downloaded = list_downloaded(mailing_list)

    auth = aiohttp.BasicAuth('archives', 'antispam')
    async with aiohttp.ClientSession(auth=auth) as session:
        mboxes = await list_of_mboxes(session, mailing_list)

    for mbox in mboxes:
        if mbox not in downloaded or downloaded[mbox] == 0:
            fn = '%(list)s/%(mbox)s' % {'list': mailing_list, 'mbox': mbox}
            print("fetching... %s to %s" % (mbox, fn))
            url = 'http://www.postgresql.org/list/%(list)s/mbox/%(mbox)s' % {
                'list': mailing_list,
                'mbox': mbox
            }

            try:
                async with aiohttp.ClientSession(auth=auth) as session:
                    await fetch_file(session, url, fn)
            except aiohttp.client_exceptions.ClientError:
                continue
        else:
            print("skipping", mbox)


def download_list_mboxes(lists):
    while True:
        coroutines = []
        for mailing_list in lists:
            coroutines.append(download_mboxes(mailing_list, mailing_list))

        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.gather(*coroutines))
        loop.close()


def list_downloaded(mailing_list):
    files = {}
    if not os.path.exists(mailing_list):
        os.mkdir(mailing_list)
        return files

    for f in os.listdir(mailing_list):
        files.update({f: os.path.getsize('%s/%s' % (mailing_list, f))})

    return files


if __name__ == '__main__':
    args = parse_arguments()
    lists = []

    if args.all_lists:
        lists = user_lists + dev_lists + project_lists + inactive_lists

    if args.user_lists:
        lists += user_lists

    if args.dev_lists:
        lists += dev_lists

    if args.project_lists:
        lists += project_lists

    if args.inactive_lists:
        lists += inactive_lists

    if args.lists:
        lists += args.lists

    download_list_mboxes(set(lists))
