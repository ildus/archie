#!/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import cgi
import datetime
import psycopg2
import re

import os
import sys

p = os.path.abspath(__file__).split(os.sep)[:-2]
p.append("lib")

sys.path.append(os.sep.join(p))

import makotool

import archie

# FIXME not a good thing I guess
reload(sys)
sys.setdefaultencoding('utf-8')

def handle_error():
	cherrypy.response.status = 500
	cherrypy.response.body = ["<html><body>Sorry, an error occured<pre>" + cherrypy._cperror.format_exc() + "</pre></body></html>"]

# homepage
class Archie(object):

	_cp_config = {'request.error_response': handle_error}
	   
	def __init__(self):
		self.pool = None
		self.archie = archie.ArchiveSearch(self.pool)
    
	@cherrypy.expose
	@cherrypy.tools.mako(filename="index.html")
	def index(self):
		"""renders the homepage"""
		return {}
        
	@cherrypy.expose
	@cherrypy.tools.mako(filename="search.html")
	def search(self, q, threads = False, lists = None, date_from = None, date_to = None):
		"""renders the search results page - either for individual messages or threads"""
		
		try:
			
			dfrom = ''
			dto   = ''
			
			if date_from:
				dfrom = datetime.datetime.strptime(date_from, '%m/%d/%Y')
			
			if date_to:
				dto = datetime.datetime.strptime(date_to, '%m/%d/%Y')
			
			# search for threads or individual messages			
			if threads:
				results = self.highlight_all(self.archie.search_threads(q, 100, dfrom, dto), escape=True)
				return {'items' : results, 'q' : q, 'threads' : True, 'date_from' : date_from, 'date_to' : date_to}
			
			else:
				results = self.highlight_all(self.archie.search(q, 100, dfrom, dto), escape=True)
				return {'items' : results, 'q' : q, 'threads' : False, 'date_from' : date_from, 'date_to' : date_to}
		
		except Exception as e:
			# FIXME log and redirect to error page
			cherrypy.log.error("Error when searching for '%s'" % (q,), traceback=False)
			raise

	@cherrypy.expose
	@cherrypy.tools.mako(filename="message.html")
	def message(self, id, q = ''):
		"""renders single message by ID (artificial key) - message itself, references and thread"""
		
		try:
			
			if q != '':
				message = self.archie.get_message_by_id(id, q)
			else:
				message = self.archie.get_message_by_id(id)
			
			message = self.highlight(message)
			
			refs = self.archie.get_references(id)
			thread = self.archie.get_thread(message['thread_id'])
			
			return {'message' : message, 'references' : refs, 'thread' : thread, 'q' : q, 'date_from' : '', 'date_to' : ''}
			
		except Exception as e:
			# FIXME log and redirect to error page
			cherrypy.log.error("Error when fetching message '%s'" % (id,), traceback=False)
			raise

	@cherrypy.expose
	@cherrypy.tools.mako(filename="raw.html")
	def raw(self, id, q = ''):
		"""renders single message by ID (artificial key) - raw body"""
		
		try:
			message = self.archie.get_raw_message_by_id(id)
			
			cherrypy.response.headers['Content-Type']= 'text/plain'
			
			return {'message' : message['raw_message']}
			
		except Exception as e:
			# FIXME log and redirect to error page
			cherrypy.log.error("Error when fetching message '%s'" % (id,), traceback=False)
			raise

	@cherrypy.expose
	@cherrypy.tools.mako(filename="thread.html")
	def thread(self, id, q = ''):
		"""renders a single thread, identified by an ID of the first message"""
		
		try:
			
			messages = self.highlight_all(self.archie.get_thread(id, q), escape=True)
			
			return {'messages' : messages, 'q' : q, 'date_from' : '', 'date_to' : ''}
			
		except Exception as e:
			# FIXME log and redirect to error page
			cherrypy.log.error("Error when fetching thread '%s'" % (id,), traceback=False)
			raise

	def highlight_all(self, messages, escape=False):
		"""highlights search terms in all the messages"""
		
		if messages == None:
			return None
		
		if escape:
			messages = self.escape_all(messages)
		
		for m in messages:
			if m['body_plain']:
				m['body_plain'] = re.sub('\[highlight\]', '<span class="highlight">', m['body_plain'])
				m['body_plain'] = re.sub('\[/highlight\]', '</span>', m['body_plain'])
		
		return messages

	def highlight(self, message, escape=False):
		"""highlights search terms in one message"""
		
		if message == None:
			return None
		
		if escape:
			message = self.escape(message)
		
		if message['body_plain']:
			message['body_plain'] = re.sub('\[highlight\]', '<span class="highlight">', message['body_plain'])
			message['body_plain'] = re.sub('\[/highlight\]', '</span>', message['body_plain'])
			
		return message

	def escape_all(self, messages):
		"""escape special characters in all the messages"""
		
		for m in messages:
			if m['body_plain']:
				m['body_plain'] = cgi.escape(m['body_plain'])
		
		return messages

	def escape(self, message):
		"""escape special characters in a single message"""
		
		if message['body_plain']:
			message['body_plain'] = cgi.escape(message['body_plain'])
		
		return message

cherrypy.quickstart(Archie(), config="server.conf")
