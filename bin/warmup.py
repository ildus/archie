#!/bin/env python

import argparse
import datetime
import getpass
import multiprocessing
import os
import psycopg2
import psycopg2.extras
import random
import re
import sys

from multiprocessing import Process, Queue

def run_warmup(id, connstr, results, duration, test_hash, test_thread, nwords):
	
	conn = psycopg2.connect(connstr)
	conn.set_session('DEFAULT', True, False, True)
	
	cur = conn.cursor()
	
	# fetch min/max ID, so that we can shoot randomly with higher probability
	cur.execute("SELECT MIN(id), MAX(id) FROM messages")
	r = cur.fetchone()
	
	n_messages = 0
	n_words = 0
	
	# get start time of the benchmark
	start_time = datetime.datetime.now()
	
	# repeat until we reach the duration limit
	while ((datetime.datetime.now() - start_time).total_seconds() < duration):
		
		n_messages += 1
		
		# get random ID and try to fetch that message
		id = random.randrange(r[0],r[1])
		cur.execute("SELECT thread_id, hash_id, body_tsvector FROM messages WHERE id = %s", (id,))
		h = cur.fetchone()
		
		if not h:
			# message with that ID does not exist - end
			continue
		
		if test_thread:
			cur.execute("SELECT hash_id FROM messages WHERE thread_id = %s", (h[0],))
		
		if test_hash:
			cur.execute("SELECT hash_id FROM messages WHERE hash_id = %s", (h[1],))
		
		if (nwords > 0) and h[2]:
			words = re.findall("'([^']*)':[0-9]+", h[2])
			
			for i in xrange(len(words)-nwords+1):
				if ((datetime.datetime.now() - start_time).total_seconds() > duration):
					# terminate if the duration is over
					break
				
				q = " ".join(words[i:(i+nwords)])
				cur.execute("SELECT id FROM messages WHERE body_tsvector @@ plainto_tsquery('english', %s)", (q,))
				n_words += 1
	
	cur.close()
	conn.close()
	
	results.put({'messages' : n_messages, 'words' : n_words,'duration' : (datetime.datetime.now() - start_time).total_seconds()})

def build_parser():
	
	parser = argparse.ArgumentParser(description='Archie benchmarking / warming-up tool')
	
	parser.add_argument('--duration', dest='duration', action='store', type=int,
						default=60, help='test duration (60 seconds by default)')
	
	parser.add_argument('--no-hash', dest='test_hash', action='store_false',
						default=True, help='disable access through message hash ID')
	
	parser.add_argument('--no-thread', dest='test_thread', action='store_false',
						default=True, help='disable access through thread ID')
	
	parser.add_argument('--words', dest='nwords', action='store', type=int,
						default=1, help='number of words to use for full-text queries')
	
	parser.add_argument('--workers', dest='nworkers', action='store', type=int,
						default=0, help='number of worker processes (0 - number of cores)')
	
	parser.add_argument('--host', dest='hostname', action='store', type=str,
						default='localhost', help='database server hostname (localhost by default)')
	
	parser.add_argument('--db', dest='dbname', action='store', type=str,
						required=True, help='name of the database to test (required)')
	
	parser.add_argument('--port', dest='port', action='store', type=int,
						default=5432, help='database server port (5432 by default)')
	
	parser.add_argument('--user', dest='user', action='store', type=str,
						default=getpass.getuser(), help='db username (current OS user by default)')
	
	parser.add_argument('--password', dest='password', action='store', type=str,
						default=None, help='db password (empty by default)')
	
	return parser

if __name__ == '__main__':
	
	parser = build_parser()
	
	args = parser.parse_args()
	
	# get number of workers
	nworkers = multiprocessing.cpu_count()
	if args.nworkers > 0:
		nworkers = args.nworkers
	
	results = Queue()
	
	# start workers
	workers = []
	for i in xrange(nworkers):
		
		if args.password:
			connstr = 'host=%s dbname=%s user=%s port=%s password=%s' % (args.hostname,
				args.dbname, args.user, args.port, arg.password)
		else:
			connstr = 'host=%s dbname=%s user=%s port=%s' % (args.hostname,
				args.dbname, args.user, args.port)

		w = Process(target=run_warmup, args=(i, connstr, results, args.duration, args.test_hash, args.test_thread, args.nwords))
		workers.append(w)
		w.start()
	
	for w in workers:
		w.join()
	
	res = {'messages' : 0, 'words' : 0, 'duration' : 0}
	while not results.empty():
		r = results.get()
		res['messages'] += r['messages']
		res['words']    += r['words']
		res['duration']  = max(res['duration'], r['duration'])
	
	print 'nwords=%s : messages=%s words=%s duration=%s wps=%s' % (args.nwords, res['messages'], res['words'], round(res['duration'],2), round(res['words']/res['duration'],2)),