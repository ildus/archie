#!/bin/env python

import argparse
import time
import getpass
import multiprocessing
import os
import re
import sys

# database interaction
import psycopg2
import psycopg2.extras
import psycopg2.errorcodes

# message parsing etc.
import email
import email.message

from multiprocessing import Process, JoinableQueue, Lock

msglock = None


def get_headers(message):
    '''returns headers as a simple dictionary (for the hstore column)

    Fields with multiple values are joined to a single string with comma
    as a separator.'''
    headers = {}
    for k in message.keys():
        headers.update({k.lower(): ','.join(message.get_all(k))})

    return headers


def get_body(message):
    '''extracts the text body of a message for indexing/display

    First it searches for a text/plain part, then text/html and finally for
    any text/* part. The first found matching part is returned. If no such
    part is found, the function returns None.'''

    if not message.is_multipart():
        if message.get_content_type() == 'text/html':
            return clean_html(message.get_payload()).strip()
        elif message.get_content_type()[:5] == 'text/':
            # handles text/plain, text/enriched etc,
            return message.get_payload().strip()

        # unknown content-type (not a text, so we can't handle it)
        return None

    # OK, so now we have to deal with multi-part messages - let's search for
    # text parts - plaintext first, then HTML and finally any text
    for m in message.get_payload():
        if m.get_content_type() == 'text/plain':
            return m.get_payload().strip()

    for m in message.get_payload():
        if m.get_content_type() == 'text/html':
            return clean_html(m.get_payload()).strip()

    for m in message.get_payload():
        if m.get_cotent_type()[:5] == 'text/':
            return m.get_payload().strip()


def message_date(message):
    '''extracts date of when the message was sent'''

    l = message.as_string(True).split("\n")[0]

    r = re.match(
        '^From .*@.*\s+([a-zA-Z]*\s+[a-zA-Z]*\s+[0-9]+ [0-9]+:[0-9]+:[0-9]+\s+[0-9]{4})$',
        l)
    if r:
        return r.group(1)

    r = re.match(
        '^From bouncefilter\s+([a-zA-Z]*\s+[a-zA-Z]*\s+[0-9]+ [0-9]+:[0-9]+:[0-9]+\s+[0-9]{4})$',
        l)
    if r:
        return r.group(1)

    if message['date'] is None:
        return None

    date = str(message['date'])
    r = re.search('^([^+-]*) [+-].*$', date)
    if r:
        return r.group(1)

    r = re.search('^([^\(]*) \(.*', date)
    if r:
        return r.group(1)

    return date or None


def clean_html(raw_html):
    '''removes all HTML tags from a HTML message'''

    return re.sub(re.compile('<.*?>'), '', raw_html)


def load_files(id, connstr, queue, started):
    '''gets list of files (a task) from the queue and loads them'''

    conn = psycopg2.connect(connstr)
    conn.set_client_encoding('LATIN1')
    psycopg2.extras.register_hstore(conn)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    c = 0
    n = 0
    start_time = time.time()
    multi = not isinstance(queue, list)

    try:
        while True:
            if not multi:
                try:
                    task = queue.pop()
                except IndexError:
                    task = None
            else:
                task = queue.get()

            if not task:
                d = (time.time() - start_time)

                msglock.acquire()
                print("%8s sec : worker %2s : no more tasks, terminating ..." %
                      (round(d, 1), id))
                sys.stdout.flush()
                msglock.release()

                if multi:
                    queue.task_done()
                break

            cur.execute(
                "SELECT file_name, file_size FROM files WHERE list = %(list)s",
                {
                    'list': task['list']
                })
            results = cur.fetchall()

            files = {}
            for r in results:
                files.update({r['file_name']: r})

            for f in task['files']:
                if os.path.basename(f) in files and (files[os.path.basename(
                        f)]['file_size'] == os.path.getsize(f)):
                    continue

                (msgs, errs, dups) = load_mbox(task['list'], f, cur)
                if os.path.basename(f) in files:
                    cur.execute(
                        """UPDATE files
                                      SET messages_ok = messages_ok + %(ok)s,
                                          messages_err = messages_err + %(err)s,
                                          file_size = %(size)s,
                                          loaded_date = now()
                                    WHERE file_name = %(file)s""", {
                            'ok': msgs,
                            'err': errs,
                            'size': os.path.getsize(f),
                            'file': os.path.basename(f)
                        })
                else:
                    cur.execute(
                        """INSERT INTO files (list, file_name, file_size, messages_ok, messages_err, loaded_date)
                                   VALUES (%(list)s, %(file)s, %(size)s, %(ok)s, %(err)s, now())""",
                        {
                            'list': task['list'],
                            'ok': msgs,
                            'err': errs,
                            'size': os.path.getsize(f),
                            'file': os.path.basename(f)
                        })
                conn.commit()
                n += msgs
                d = (time.time() - start_time)
                msglock.acquire()
                print(
                    "%8s sec : worker %2s : %-26s %4s messages (%4s errors, %4s duplicates) : %s msgs/sec"
                    % (round(d, 1), id, os.path.basename(f), msgs, errs, dups,
                       round((c + n) / d, 1)))
                sys.stdout.flush()
                msglock.release()

                # analyze only if we reach enough new messages (at least 250 and more that inserted so far)
                if (n >= c) and (n >= 250):
                    c += n
                    n = 0
                    cur.execute('ANALYZE messages')

            if multi:
                queue.task_done()
    except KeyboardInterrupt:
        print("worker %s : process stopped by Ctrl-C" % (id, ))
    finally:
        cur.close()
        conn.close()


def load_mbox(lname, filename, cur):
    '''loads a single mbox file and inserts the messages into the database'''
    r = re.match('(.*)\.[0-9]{6}', os.path.basename(filename))
    if r:
        lst = r.group(1)
        if lst != lname:
            print("ERROR: mbox '%s' does not belong to list '%s'" % (filename,
                                                                     lname))
            return
    else:
        print("unknown list (expected %s): %s" % (filename, lname))
        return

    msgs = read_messages(filename)

    cur.execute("SAVEPOINT bulk_load")

    n_msgs = 0
    n_errs = 0
    n_dups = 0

    for m in msgs:
        try:
            mid = None
            if m['message-id']:
                r = re.search('<(.*)>.*', m['message-id'])
                if r:
                    mid = r.group(1)

            date = message_date(m)
            subject = m['subject']
            if subject:
                subject = subject.strip()
            refs = None

            if m['references']:
                refs = re.findall('<([^>]*)>', m['references'])

            in_reply_to = None

            if m['in-reply-to']:
                in_reply_to = re.findall('<([^>]*)>', m['in-reply-to'])

            body = m.get_payload()

            if m['message-date'] == '':
                continue
            cur.execute(
                "INSERT INTO messages (list, message_id, in_reply_to, refs, sent, subject, author, body_plain, headers, raw_message) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                (lst, mid, in_reply_to, refs, date, subject, m['from'],
                 get_body(m), get_headers(m), m.as_string(True)))
            cur.execute("SAVEPOINT bulk_load")
            n_msgs += 1
        except psycopg2.DatabaseError as ex:
            # well, no matter what happened, rollback to the savepoint
            cur.execute("ROLLBACK TO bulk_load")
            if ex.pgcode == psycopg2.errorcodes.UNIQUE_VIOLATION:
                n_dups += 1
            else:
                print(ex.pgcode, ':', ex.pgerror)
                n_errs += 1
        except Exception as e:
            cur.execute("ROLLBACK to bulk_load")
            print("unknown exception: %s" % e)

    return (n_msgs, n_errs, n_dups)


def read_messages(filename):
    '''reads the file, splits it into messages and parses them using the email module'''

    messages = []
    message = ''
    with open(filename, 'r') as f:
        for line in f.readlines():
            if re.match(
                    '^From .*@.*\s+[a-zA-Z]*\s+[a-zA-Z]*\s+[0-9]+ [0-9]+:[0-9]+:[0-9]+\s+[0-9]{4}$',
                    line):
                if message != '':
                    messages.append(email.message_from_string(message))
                message = line
            elif re.match(
                    '^From bouncefilter\s+[a-zA-Z]*\s+[a-zA-Z]*\s+[0-9]+ [0-9]+:[0-9]+:[0-9]+\s+[0-9]{4}$',
                    line):
                if message != '':
                    messages.append(email.message_from_string(message))
                message = line
            elif re.match('^From scrappy$', line):
                if message != '':
                    messages.append(email.message_from_string(message))
                message = line
            else:
                message += line

    messages.append(email.message_from_string(message))
    return messages


def get_space(files):
    '''returns total space occupied by the files (in bytes)'''
    s = 0
    for f in files:
        s += os.path.getsize(f)
    return s


def build_parser():
    '''builds a command-line options parser'''

    parser = argparse.ArgumentParser(description='Archie loader')
    parser.add_argument(
        'files',
        metavar='FILE',
        type=str,
        nargs='+',
        help='mbox files to load')
    parser.add_argument(
        '--workers',
        dest='nworkers',
        action='store',
        type=int,
        default=0,
        help='number of worker processes (default is number of cores)')
    parser.add_argument(
        '--host',
        dest='hostname',
        action='store',
        type=str,
        default='localhost',
        help='database server hostname (localhost by default)')
    parser.add_argument(
        '--db',
        dest='dbname',
        action='store',
        type=str,
        required=True,
        help='name of the database to test (required)')
    parser.add_argument(
        '--port',
        dest='port',
        action='store',
        type=int,
        default=5432,
        help='database server port (5432 by default)')
    parser.add_argument(
        '--user',
        dest='user',
        action='store',
        type=str,
        default=getpass.getuser(),
        help='db username (current OS user by default)')
    parser.add_argument(
        '--password',
        dest='password',
        action='store',
        type=str,
        default=None,
        help='db password (empty by default)')

    return parser


if __name__ == '__main__':
    # build parser and parse arguments
    parser = build_parser()
    args = parser.parse_args()
    msglock = Lock()

    if args.password:
        connstr = 'host=%s dbname=%s user=%s port=%s password=%s' % (
            args.hostname, args.dbname, args.user, args.port, args.password)
    else:
        connstr = 'host=%s dbname=%s user=%s port=%s' % (args.hostname,
                                                         args.dbname,
                                                         args.user, args.port)

    # prepare list of files
    lists = {}
    for f in args.files:
        r = re.match('([a-z-]*)\.[0-9]{6}', os.path.basename(f))
        if r:
            mlist = r.group(1)
            if mlist not in lists:
                lists.update({mlist: [f]})
            else:
                lists[mlist].append(f)

    # get number of workers
    nworkers = multiprocessing.cpu_count()
    if args.nworkers > 0:
        nworkers = args.nworkers

    if nworkers > len(lists):
        nworkers = len(lists)

    # get occupied space for each list (assumption space ~ number of messages)
    keys = [{'key': k, 'length': get_space(lists[k])} for k in lists.keys()]
    keys = sorted(keys, key=lambda x: -x['length'])

    if nworkers > 1:
        queue = JoinableQueue()
        for k in keys:
            queue.put({'list': k['key'], 'files': lists[k['key']]})
    else:
        queue = []
        for k in keys:
            queue.append({'list': k['key'], 'files': lists[k['key']]})

    try:
        started = time.time()

        if nworkers > 1:
            # start workers
            workers = []
            for i in range(nworkers):
                p = Process(target=load_files, args=(i, connstr, queue, started))
                workers.append(p)
                p.start()
                # for each worker, put there one empty message, meaning "stop"
                queue.put(False)

            # wait for the workers to finish
            queue.join()
        else:
            # just run in current process
            load_files(0, connstr, queue, started)

        runtime = (time.time() - started)
        print("total runtime: %s seconds" % (round(runtime, 1), ))

    except KeyboardInterrupt:
        print("loading interrupted")
